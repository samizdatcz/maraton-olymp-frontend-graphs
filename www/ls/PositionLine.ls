stories =
  * title: "Nervozita na startu"
    content: "Před startem je u našeho běžce Alexe vidět mírná nervozita: tepová frekvence 100 úderů za minutu. Rozbíhá se deset minut po deváté a tep mu rychle vyletí na 135. Tělo se adaptuje na sportovní výkon. Potředuje na to přibližně pět kilometrů. Pak už je tepová frekvence na maximu okolo 180 úderů za minutu. „Běžec se nechal strhnout rychlostí ostatních, přepálil začátek, kdy běžel mezi 8 a 11 kilometry v hodině. To by znamenalo zaběhnout celý maraton okolo čtyř hodin. To nebylo reálně v jeho silách."
    kmOffset: 0
  * title: "Cévy se smršťují"
    content: "Na prvních kilometrech je ještě vidět dynamickou práci nohou. Dostavuje se lokální pokles průtoku krve ve svalech, cévy se smršťují. U krátkodobě trénovaného běžce je to typické, naproti tomu u zkušených sportovců není svalová komprese žádná a do svalu přitéká stále stejné, ne-li větší množství krve."
    kmOffset: 4
  * title: "Svaly nezvládají zužitkovat kyslík"
    content: "Nárůst tepové frekvence logicky znamená větší srdeční výdej, tedy množství srdcem přečerpané krve. Alexovy svaly, trénované jen půl roku, ale nejsou schopné zvýšenou dodávku krve, a tedy i kyslíku, zpracovat. V modrém grafu vidíme, jak roste okysličení svalu. U špičkových sportovců je trend přesně opačný, okysličení (SmO<sub>2</sub>) jim po startu klesá, jelikož svaly kyslík spotřebovávají."
    kmOffset: 9
  * title: "Vysoká koncentrace laktátu"
    content: "Vysoká je i koncentrace laktátu v krvi (5 mmol/l na 11. kilometru). Svaly nestačí sůl kysleiny mléčné spotřebovávat. Je jasné, že běžec bude muset zpomalit. Mezi 11. a 20. kilometrem Alexova rychlost skutečně klesá. Zpomalil v průměru o 2 km/h a snížila se i koncentrace laktátu (2&nbsp;mmol/l na 21. km). Podle klasické teorie by to znamenalo, že je běžec v optimálním tempu aerobní zátěže a mohl by pokračovat dále, či dokonce lehce zrychlit."
    kmOffset: 11
  * title: "Svaly nezvládají zužitkovat kyslík"
    content: "Jenže klasická teorie se ne vždy potvrdí. Tentokrát se běžcovy mitochondrie ani po zpomalení nevzpamatovaly, schopnost svalů využívat kyslík dál klesala. Okysličení svalu na 11. km bylo kolem 87 %, na 21. km okolo 92 %. Okysličení krve je přitom téměř 100 %. Svaly tedy dokázaly spotřebovat méně než 8 % kyslíku dodávaného krví. Tělo pak udržovalo vysokou tepovou frekvenci ve snaze metabolismus kyslíku ve svalech zvýšit. Vrcholoví sportovci spotřebují přibližně polovinu krví dodávaného kyslíku, okysličení jejich svalů se pohybuje kolem 65 %."
    kmOffset: 18
  * title: "Krize"
    content: "Mezi 20. a 30. kilometrem si Alex sáhl na dno. Okysličení svalu stouplo na 94 %. Energii bral prakticky jen anaerobně, to znamená, že spaloval především sacharidy. Běžec proto opět zpomalil, místy šel či se úplně zastavoval. Tepová frekvence proto klesla ke 130. Laktát stoupl na 5 mmol/l. Klasická tréninková rada by zněla: zpomalit. Jenže s dalším poklesem tepové frekvence by ubyl i kyslík ve svalech a zanedlouho by vyčerpání běžce vyřadilo. Proto mu lékař poradil, aby se pokusil zrychlit a pokračoval, a to indiánským během, tedy střídáním klusu a chůze."
    kmOffset: 25
  * title: "Úskalí indiánského běhu"
    content: "Alex radu lékaře poslechl, tedy alespoň ze začátku. Po 31. kilometru klesá okysličení svalů na 84 %, naopak průtok krve stoupá. I chůzí prokládané běhy ale byly příliš dlouhé. Neostatečně trénované svaly tak opět přestaly zvládat přísun kyslíku a běžec musel znovu zpomalit. Na 38. kilometru se rozhodl pokračovat, tentokrát s lépe rozloženými intervaly. Sval opět začal zpracovávat kyslík a dobře se prokrvil, takže Alex mohl zrychlit až na 8 km/h."
    kmOffset: 32
  * title: "Do cíle silou vůle"
    content: "Svaly díky tomu (jak tvrdí teorie laktátového přenosu) začaly spotřebovávat laktát vyplavený v krvi, ten tedy na 40. kilometru klesl na hodnotu 3,6 mmol/l. V cíli zdravotní sestra naměřila dokonce hodnotu 1,9 mmol/l. Ač okolo 30. kilometru hrozilo, že Alex ze závodu odstoupí, nakonec se překonal a závod silou vůle zvládl v čase šesti hodin."
    kmOffset: 40


height = ((window.innerHeight - 40) / 8) * 2
class ig.PositionLine
  (@parentContainer) ->
    ig.Events @
    @element = @parentContainer.append \div
      ..attr \class "graph position-line"
      ..style \height "#{height}px"
    @bigNumberContainer = @element.append \div
      ..attr \class \big-number-container
      ..append \div .attr \class \big-number-background
    @bigNumberTexts = @bigNumberContainer.append \div
      ..attr \class \big-number-texts
    @bigNumber = @bigNumberTexts.append \div
      ..attr \class \big-number
      ..html "0,0"
    @bigNumberUnit = @bigNumberTexts.append \div
      ..attr \class \unit
      ..html "km"
    features = ig.data.trasa.features
    map = ig.utils.geo.getFittingProjection features, {width: 300, height: height}
    @projection = map.projection
    path = d3.geo.path!
      ..projection map.projection
    @mapContainer = @bigNumberContainer.append \div
      ..attr \class "map-container"
    @mapSvg = @mapContainer.append \svg
      ..attr \width map.width
      ..attr \height map.height
      ..selectAll \path .data features .enter!append \path
        ..attr \d path
    @mapHighlightPoint = @mapSvg.append \circle
      ..attr \r 3
      ..attr \cx 19.30489191185552
      ..attr \cy 28.94522400284768
    @storyContainer = @element.append \div
      ..attr \class \story
    @storyContent = @storyContainer.append \div
      ..attr \class \content
    @storyContainer.append \div
      ..attr \class \arrows
      ..append \a
        ..html "Předchozí"
        ..on \click ~>
          @move -1
          d3.event.preventDefault!
      ..append \a
        ..html "Další"
        ..on \click ~>
          @move +1
          d3.event.preventDefault!
    @currentStoryIndex = 0
    positionDotsContainer = @element.append \div
      ..attr \class \position-dots-container
    @positionDots = positionDotsContainer.selectAll \div .data stories .enter!append \div
    @setStory!

  toHtml: (story) ->
    "<h2>#{story.title}</h2><p>#{story.content}</p>"

  update: (datapoint) ->
    str = datapoint.kmHuman
    @bigNumber.html str
    if datapoint.lat and datapoint.lon
      center = @projection [datapoint.lon, datapoint.lat]
      @mapHighlightPoint
        ..attr \cx center.0
        ..attr \cy center.1

  move: (dir) ->
    @currentStoryIndex += dir
    @currentStoryIndex %%= stories.length
    @setStory!

  setStory: ->
    story = stories[@currentStoryIndex]
    @positionDots.classed \active (d, i) ~> i is @currentStoryIndex
    @storyContent.html @toHtml story
    @emit \story story.kmOffset
