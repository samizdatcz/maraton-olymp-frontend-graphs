alldata = null

toFloats = (row) ->
  for field, value of row
    row[field] = if value
      parseFloat value
    else
      null
  row

toDouble = ->
  s = it.toString!
  if s.length == 1
    s = "0#s"
  s

data = d3.tsv.parse ig.data.data, toFloats
lastSpd = null
for datum in data
  if datum.spd == lastSpd
    datum.spd = null
  else
    lastSpd = datum.spd
  datum.kmCorrected = (datum.km - 0.6) / 44.2 * 42.5
  datum.kmHuman = if datum.kmCorrected
    that.toFixed 1 .replace '.' ','
  else
    "—"
alldata := data


body = d3.select ig.containers.base


container = body.append \div
  ..attr \class \all-container
if window.location.hash == '#bigfont'
  container.classed \bigfont yes
highlighter = container.append \div
  ..attr \class "highlighter invisible"
  ..append \div
    ..attr \class \start
  ..append \div
    ..attr \class \end
hours = highlighter.append \span
  ..attr \class \hours




xLines = []
lastMod = Infinity
tempDate = new Date!
for datum, index in data
  mod = datum.time % 1800000
  if lastMod - mod > 5000 || index in [522 4945]
    tempDate.setTime datum.time
    datum.timeHuman = "#{toDouble tempDate.getHours!}:#{toDouble tempDate.getMinutes!}:#{toDouble tempDate.getSeconds!}"
    xLines.push datum
  lastMod = mod
xLines
  ..shift!
  ..shift!
  ..shift!
  ..pop!
  ..pop!


positionLine = new ig.PositionLine container
graphs =
  new ig.Graph container, {field: "hb", unit: "úderů / min" format: -> it}, xLines
  new ig.Graph container, {field: "mox", unit: "okysličení svalu" format: -> it.toFixed 1 .replace '.' ','}, xLines
  new ig.Graph container, {field: "mox2", unit: "průtok krve" format: -> it.toFixed 2 .replace '.' ','}, xLines
  new ig.Graph container, {field: "spd", unit: "km/h", startAtZero: yes, format: -> (it * 3.6).toFixed 1 .replace '.' ','}, xLines
  new ig.Graph container, {field: "cad", unit: "kroků/min", format: -> it}, xLines
  new ig.Graph container, {field: "alt" unit: "m", format: -> it.toFixed 2 .replace '.' ','}, xLines
nowDate = new Date!
for graph in graphs
  graph.on \mousemove (pxX, time, datapoint) ->
    for graph in graphs
      graph.setMouseMoveHighlight pxX
    highlighter
      ..classed \invisible no
      ..style \left "#{pxX}px"
    container.classed \highlighter-visible yes
    nowDate.setTime time
    hours.html "#{toDouble nowDate.getHours!}:#{toDouble nowDate.getMinutes!}:#{toDouble nowDate.getSeconds!}"
    if datapoint
      positionLine.update datapoint
  graph.on \mouseout ->
    for graph in graphs
      graph.setHighlight null
    highlighter.classed \invisible yes
    container.classed \highlighter-visible no

bigNumbers = container.selectAll \.scrollable-numbers

onScroll = ->
  left = (document.body.scrollLeft || document.documentElement.scrollLeft)
  left += window.innerWidth - 200
  bigNumbers.style \left "#{left}px"
window.addEventListener \scroll onScroll
onScroll!

for graph in graphs
  graph.update data

positionLine.on \story (kmOffset) ->
  datapoint = null
  for datum in data
    if datum.kmCorrected > kmOffset
      datapoint = datum
      break
  positionLine.update datapoint
  scrollX = (graphs.0.toX datapoint) - ((window.innerWidth - 300) / 2)
  d3.transition!
    .duration 800
    .tween "scroll" scrollTween scrollX


scrollTween = (offset) ->
  ->
    interpolate = d3.interpolateNumber do
      window.pageXOffset || document.documentElement.scrollLeft
      offset
    (progress) -> window.scrollTo (interpolate progress), 0
