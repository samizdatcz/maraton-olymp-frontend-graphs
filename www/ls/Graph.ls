maxLength = 3600 * 8 * 1e3
startTime = null
height = (window.innerHeight - 40) / 8
class ig.Graph
  (@parentContainer, @setup, @xLines) ->
    @data = []
    @pathData = []
    ig.Events @
    @height = height
    @width = 5500
    @container = @parentContainer.append \div
      ..attr \class "graph #{@setup.field}"
    if @setup.field is "hb"
      @container.style \margin-top "#{height * 2}px"
    @svg = @container.append \svg
      ..attr \width @width
      ..attr \height @height
    @drawing = @svg.append \g
      ..attr \class \drawing
      ..attr \transform "translate(0, 20)"
    @pathG = @drawing.append \g
      ..attr \class \path
    @path = @pathG.append \path
    @circleG = @drawing.append \g
      ..attr \class \circle
    @highlightCircle = @circleG.append \circle
      ..attr \r 3
    @scaleX = d3.scale.linear!
      ..domain [0 maxLength]
      ..range [0 @width]
    @scaleY = d3.scale.linear!
    yRange = [0 @height - 40]
    if !@setup.invertAxis then yRange.reverse!
    @scaleY.range yRange
    @pathGenerator = d3.svg.line!
    @bigNumberContainer = @container.append \div
      ..attr \class "big-number-container scrollable-numbers"
      ..append \div .attr \class \big-number-background
    @bigNumberTexts = @bigNumberContainer.append \div
      ..attr \class \big-number-texts
    @bigNumber = @bigNumberTexts.append \div
      ..attr \class \big-number
    @bigNumberUnit = @bigNumberTexts.append \div
      ..attr \class \unit
      ..html @setup.unit
    @svg.on \mousemove @~onMouseMove
    @svg.on \mouseout ~> @emit \mouseout
    @manualHighlight = no
    @xLineContainer = @container.append \div
      ..attr \class \x-lines
    @xLineElements = @xLineContainer.selectAll \div .data @xLines .enter!append \div
      ..attr \class \x-line
      ..append \span
        ..attr \class \km
        ..html -> "#{it.kmHuman} km"
      ..append \span
        ..attr \class \time
        ..html (.timeHuman)

  update: (data) ->
    data .= filter ~> it.time isnt null and it[@setup.field] isnt null
    if @setup.field in ["mox" "mox2"]
      data .= filter -> it.mox2 < 50
    if @setup.field == \cad
      data .= filter -> it.cad < 110
    return unless data.length
    if startTime is null
      startTime := data.0.time
    yExtent = d3.extent data.map ~> it[@setup.field]
    @scaleY.domain if @setup.startAtZero
      [0, yExtent.1]
    else
      yExtent

    timeDiff = data[*-1].time - startTime
    xMax = @scaleX timeDiff
    @data = data
    @pathData = pathData = data.map ~>
      x = @scaleX it.time - startTime
      y = @scaleY it[@setup.field]
      [x, y]

    @path.attr \d @pathGenerator pathData
    if !@manualHighlight
      @setHighlight null
    @xLineElements.style \left ~> "#{@scaleX it.time - startTime}px"

  toX: (datapoint) ->
    @scaleX datapoint.time - startTime

  onMouseMove: ->
    x = d3.event.layerX
    time = startTime + @scaleX.invert d3.event.layerX
    for [x, y], index in @pathData
      if x > d3.event.layerX
        break
    @emit do
      \mousemove
      d3.event.layerX
      Math.round time
      @data[index]

  setMouseMoveHighlight: (targetX) ->
    return unless @pathData.length
    for [x, y], index in @pathData
      if x > targetX
        break
    @setHighlight index


  setHighlight: (index) ->
    if index != null
      @manualHighlight = yes
    else
      @manualHighlight = no
      index = @data.length - 1
    return if index < 0
    @circleG.attr \transform "translate(#{@pathData[index].join ','})"
    @setBigNumber @data[index][@setup.field]

  setBigNumber: (value) ->
    @bigNumber.html @setup.format value
